<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>MOBILIARIO DE INFORMATICA</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/index1.css">   
</head>
<body>
    <header>
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navegacion-fm">
                        <span class="sr-only">Desplegar / Ocultar Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Inicia Menu -->
                <div class="collapse navbar-collapse" id="navegacion-fm">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Inicio</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Contacto</a></li>
                    </ul>

                    <div class="navbar-right" id="navegacion-fm">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="login.php">Ingresar</a></li>
                        <li><a href="usuario.php">Registrarse</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <section class="jumbotron">
        <div class="container">
            <h2 class="text-center">SISTEMA DE SEGUIMIENTO ACADÉMICO</h2>
        </div>
    </section>

    <section class="main container">
        <div class="row">
            <section class="posts col-md-9">
                <div class="miga-de-pan">
                    <ol class="breadcrumb">
                        <li><a href="#">Inicio</a></li>
                    </ol>
                </div>

                <article class="post clearfix">
                    <a href="#" class="thumb pull-left">
                        <img class="img-thumbnail" src="img/e2.jpg" alt="">
                    </a>
                    <h2 class="post-title">
                        <a href="#">Quienes Somos</a>
                    </h2>
                    <p><span class="post-fecha">26 de Enero de 2018</span> por <span class="post-autor"><a href="#">andres Reyes</a></span></p>
                    <p class="post-contenido text-justify">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>

                    <div class="contenedor-botones">
                        <a href="#" class="btn btn-primary b">Leer Mas</a>
                        
                    </div>
                </article>

                <article class="post clearfix">
                    <a href="#" class="thumb pull-left">
                        <img class="img-thumbnail" src="img/e3.jpg" alt="">
                    </a>
                    <h2 class="post-title">
                        <a href="#">Reseña Historica</a>
                    </h2>
                    <p><span class="post-fecha">26 de Enero de 2018</span> por <span class="post-autor"><a href="#">Pablo Perez</a></span></p>
                    <p class="post-contenido text-justify">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>

                    <div class="contenedor-botones">
                        <a href="#" class="btn btn-primary">Leer Mas</a>
                        
                    </div>
                </article>
                <nav>
                    <div class="center-block">
                        <ul class="pagination">
                            <li class="disabled"><a href="#">&laquo;<span class="sr-only">Anterior</span></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">&raquo; <span class="sr-only">Siguiente</span></a></li>
                        </ul>
                    </div>
                </nav>
            </section>

            <aside class="col-md-3 hidden-xs hidden-sm">
                <h4>Categorias</h4>
                <div class="list-group">
                    <a href="#" class="list-group-item active">Niveles</a>
                    <a href="#" class="list-group-item">PRIMARIA</a>
                    <a href="#" class="list-group-item">QASHQAI</a>
                </div>

                <h4>Quienes Somos</h4>
                <a href="#" class="list-group-item">
                    <center><h4 class="list-group-item-heading">U.E Gral. JOSE MANUEL PANDO</h4></center>
                    <p class="list-group-item-text">Con el Audi R8 se ha conseguido crear un automóvil de dos puertas tan refinado como un lujoso…</p>
                </a>

                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">EL MEJOR MUSCLE CAR</h4>
                    <p class="list-group-item-text">Más allá de su motor, el GT350R se diferencia del Mustang regular por una transmisión manual Tremec de 6 velocidades.</p>
                </a>

                <a href="#" class="list-group-item">
                    <h4 class="list-group-item-heading">Conoce el carro nuevo más caro del mundo</h4>
                    <p class="list-group-item-text">Con un costo de 17.5 millones de dólares este súper deportivo se apodera del crédito de ser el carro nuevo más caro de todos los tiempos.</p>
                </a>
            </aside>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-6">
                    <ul class="list-inline text-right">
                        <li><a href="#">Inicio</a></li>
                        <li><a href="#">Cursos</a></li>
                        <li><a href="#">Contacto</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>