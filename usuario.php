<?php session_start();?>
<?php include 'conex.php'; 
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <title>Tablas</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/usuario.css">

</head>
<body>

<section class="container">
  <div class="row">
  <?php 
       if (isset($_POST['submit'])){
        $user = mysqli_real_escape_string($conexion, $_POST['user']);
        $pass = mysqli_real_escape_string($conexion, $_POST['pass']);
        $skill = mysqli_real_escape_string($conexion, $_POST['skill']);
        $privilegio = mysqli_real_escape_string($conexion, $_POST['privilegio']);

        $pass = password_hash($pass, PASSWORD_DEFAULT);

        $sql_insertar = "INSERT INTO admin(user, pass, skill, privilegio)
        values('$user', '$pass', '$skill', '$privilegio')";
        $conexion->query($sql_insertar);
        mysqli_close($conexion);
        echo '<script> alert("LOS DATOS HAN SIDO GUARDADOS CORRECTAMENTE");
           self.location = "form.php" //redireccionar a formulario php       
         </script>';
       }
   ?>

    <div class="col-sm-12"></div>
    <div class="col-sm-12">
      <form action="form.php" method="POST" class="formu">
      <?php 
         if (isset($error)){
            echo "<div class='alert alert-danger'> <span>".$error."</span></div>";
      }
    ?>
   
      <h2 class="text"><center>REGISTRO USUARIO</center></h2>
      
          <div class="form-group">
          <label class="text">Usuario:(*)</label>
          <input type="email" name="user" id="user" placeholder="Indroduzca su nombre" class="form-control">
          </div>
    <div class="form-group">
    <label class="text">Password:(*)</label>
      <input type="password" name="pass" id="pass" placeholder="Indroduzca su correo" class="form-control">
    </div>
    <div class="form-group">
    <label class="text">Skill:(*)</label>
    <select name="skill" id="skill" class="form-control" required="">
      <option value="">Seleccionar lenguaje</option>
      <option value="PHP">PHP</option>
      <option value="JAVA">JAVA</option>
      <option value=".NET">NET</option>
      <option value="PYTHON">PYTHON</option>
      <option value="POSTGRE">POSTGRE</option>
    </select>
    </div>
  <div class="form-group">
    <label class="text">Cargo:(*)</label>
    <select name="privilegio" id="privilegio" class="form-control" required="">
      <option value="">Seleccionar</option>
      <option value="1">Administrador</option>
      <option value="2">Programador</option>
      <option value="3">Usuario</option>
    </select>
    </div>
   <div class="form-group">
    <button type="submit" name="submit" value="submit" class="btn btn-primary">Guardar</button>
    <button type="reset" value="Cancel" class="btn btn-success">Limpiar</button>
   </div>
   <div class="form-group">
    <span class=""><a href="index.php">Volver</a></span>
   </div>
    </form>
    </div>
  </div>
</section>

</body>
</html>