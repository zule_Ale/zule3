<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="shortcut icon" type="image/x-icon" href="img/loguito.png">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <meta name="description" content="Formulario de login">
    <meta name="KEYWORDS" content="Formulario,login,inicio de sesion">
    <link rel="stylesheet" href="css/estilo2.css">
    <title>Document</title>
</head>
<body>
   <section class="container"> 
       <div class="row">
          <div class="col-x-12 col-md-12 col-lg-12">
          <form class="login" action="person2.php" method="POST" >
            <h3><center>INGRESA</center></h3>
            
            <?php
            error_reporting(E_ALL ^ E_NOTICE);
            if($_GET["error"]=="si")
               {
                echo '<div class="alert alert-danger" role="alter"><center><strong>Ops!-Verifica tus datos.</strong></center></div>';
                }  
                  else{echo "";}
                ?>
              
              <div>
                    <div class="col-x-12 col-md-12 col-lg-12">

                    <img src="img/jose.jpg" id="avatar">
                    </div>
                    <div class="col-x-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Introduzca correo electronico" name="user" id="user" required="">
                    </div>
                    </div>
                    <div class="col-x-12 col-md-12 col-lg-12">

                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Introduzca contraseña" name="pass" id="pass" required="">
                    </div>
                    </div>
                    <div class="col-x-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg btn-block" name="entrar" value="entrar" id="boton">Iniciar sesion</button>
                        <span><a class="btn btn-danger btn-lg btn-block" href="index.php">Limpiar</a></span>
                    </div>
                    </div>
                
                </div>
                </form>
        </div>
    
    </div>
</section>

</body>
</html>