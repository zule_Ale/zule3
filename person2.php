<!DOCTYPE html>
<html>
	<head>
		<?php
		include("head.php");

    	$titu=htmlentities("Mi web", ENT_QUOTES, "UTF-8");
    	$desc=htmlentities("Descripcion de la pagina actual", ENT_QUOTES, "UTF-8");
    	$keyw='Palabra clave 1, Palabra clave 2.Palabra clave 3';
    	
    	$cano='http://www.miweb.com';
    	$img='http://www..gob.bo/res/logo.png';
    	
    	/*  */
    	$web='http://www.miweb.com';
    	$autor='Observatorio Agroambiental y Productivo - OAP';
    	

    	echo"
    	<!-- META GENERAL -->
    	<title>$titu</title>
    	<meta  name='title'       content='$titu'>
    	<meta  name='description' content='$desc'/>
    	<meta  name='keywords'    content='$keyw'>
    	<meta  name='author'      content='$autor'>
    	<meta  name='robots'      content='INDEX,FOLLOW'/>
    	<meta  name='googlebot'   content='INDEX,FOLLOW'/>
    	<link  rel='canonical'    href='$cano'>

    	<!-- META FACEBOOK -->
    	<meta property='og:locale'              content='en_EN'/>
    	<meta property='og:type'                content='website'/>
    	<meta property='og:title'               content='$titu'/>
    	<meta property='og:description'         content='$desc'/>
    	<meta property='og:url'                 content='$cano'/>
    	<meta property='og:site_name'           content='$autor'/>
    	<meta property='og:image'               content='$img'/>
    	<meta property='og:image:url'           content='$img'/>

    	<!-- META TWITTER -->
    	<meta name='twitter:title'        content='$titu'/>
    	<meta name='twitter:description'  content='$desc'/>
    	<meta name='twitter:image'        content='$img'/>
    	<meta name='twitter:site'         content='$web'/>
    	<meta name='twitter:creator'      content='$autor'/>
    	<meta name='twitter:via'          content='itsmahcode'/>
    	<meta name='twitter:card'         content='photo'/>
    	<meta name='twitter:url'          content='$cano'/>";
   	?>
	</head>

	<body>
		<?php  include("menu.php");  ?>
		<section>
        <?php
   include("conex.php");

/* GUARDAR PERSONA */
   if( isset($_POST["ap"]) )
   {   $ap=$_POST["ap"];
       $nom=$_POST["nom"];
       $ci=$_POST["ci"];
       $direc=$_POST["direc"];
       $email=$_POST["email"];
       $pwd=$_POST["pwd"];
       $telf=$_POST["telf"];
       $movil=$_POST["movil"];
       $fechn=$_POST["fechn"];
       $rude=$_POST["rude"];
       $genero=$_POST["genero"];

       if( $BDD->exec("insert into est (ap,nom,ci,direc,email,pwd,lv,telf,movil,fechn,rude,genero,stad) values ('$ap','$nom','$ci','$direc','$email','$pwd','admin','$telf','$movil',$fechn,'$rude','$genero','act')"))
       { echo"Guardado con exito";}
       else
       { echo"No se logro guardar";}
}
/*eliminar*/
       if (isset($_GET["acc"])) {
                $idest=$_GET["idest"];
                if ($BDD->exec("update est set stad='del' where  idest=$idest")) {
                    echo "se elimino";
                }
                else{echo "no se logro eliminar";}
            }
?>
       
        <button onclick="if(FORMEST.style.display=='block'){FORMEST.style.display='none';}else{FORMEST.style.display='block';}">Añadir</button>
        <article id="FORMEST" style='border:1px solid white; display: none;'>
			<h1  class='title'>Bienvenido</h1>
            <form method="POST" action="person2.php">
            <input type="text"     name="ap"       placeholder="Apellidos"><br>
            <input type="text"     name="nom"      placeholder="Nombres"><br>
            <input type="number"   name="ci"       placeholder="Carnet"><br>
            <input type="text"     name="direc"    placeholder="Direccion"><br>
            <input type="number"   name="email"    placeholder="Email"><br>
            <input type="password" name="pwd"      placeholder="Contraseña"><br>
            <input type="number"   name="telf"     placeholder="Telefono"><br>
            <input type="number"   name="movil"    placeholder="Movil"><br>
            <input type="date"     name="fechn" ><br>
            <input type="number"   name="rude"     placeholder="Cod Rude"><br>
            <input type="radio"    name="genero"   value="masculino"/>M
            <input type="radio"    name="genero"   value="femenino"/>F<br>  
            <button>Guardar</button>
            </form>
            </article>

            <article id="EDITEST" style='border:1px solid white; display: none;'>
            <h1  class='title'>Editar Estudiante</h1>
            <form method="POST" action="person2.php">
            <input type="text"     id="ap"        name="eap"        placeholder="Apellidos"><br>
            <input type="text"     id="nom"       name="enom"       placeholder="Nombres"><br>
            <input type="number"   id="ci"        name="eci"        placeholder="Carnet"><br>
            <input type="text"     id="direc"     name="edirec"     placeholder="Direccion"><br>
            <input type="text"     id="email"     name="eemail"     placeholder="Email"><br>
            <input type="password" id="pwd"       name="epwd"       placeholder="Contraseña"><br>
            <input type="number"   id="telf"      name="etelf"      placeholder="Telefono"><br>
            <input type="number"   id="movil"     name="emov"       placeholder="Movil"><br>
            <input type="date"     id="fechn"     name="efechn"><br>
            <input type="number"   id="rude"      name="erude"      placeholder="Cod Rude"><br>
            <button>Guardar</button>       
            </form>
            </article>
        <section >
            <center><h1 class='title' >Lista de Estudianes</h1></center>
            <?php 
            $MP=$BDD->query("select * from est where stad='act'");
            while($VP=$MP->fetchArray())
            {
                echo"
                <article class='card1'>
                    <h3>$VP[1] $VP[2]</h3>
                    <p>$VP[3] $VP[4] $VP[5] Telefono: $VP[8] $VP[9] $VP[10] $VP[12]</p>
                    <a style='right:0px' onclick=\"if(confirm('Esta seguro de eliminar')){location.href='person2.php?acc=del&idest=$VP[0]';}\">Eliminar</a>
                    <a style='right:50px' onclick=\"ap.value='$VP[1]'; nom.value='$VP[2]'; ci.value='$VP[3]'; direc.value='$VP[4]'; email.value='$VP[5]'; telf.value='$VP[8]';  movil.value='$VP[9]';  fechn.value='$VP[10]';  rude.value='$VP[12]'; EDITEST.style.display='block';\">Editar</a>
                </article>";
                
            }
            ?>
		</section>

		<?php  include("pie.php");  ?>
	</body>
</html>